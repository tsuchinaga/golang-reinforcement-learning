package sarsa_epsilon_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"reflect"
	"testing"
)

func TestSarsaEpsilonGreedy_GetStatus(t *testing.T) {
	testTable := []struct {
		status c.Cell
		expect c.Cell
	}{
		{c.Cell{Row: 2, Col: 2}, c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 1, Col: 2}, c.Cell{Row: 1, Col: 2}},
		{c.Cell{}, c.Cell{Row: 0, Col: 0}},
		{c.Cell{Row: 10, Col: 20}, c.Cell{Row: 10, Col: 20}},
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{Status: test.status}
		actual := agent.GetStatus()

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_SetStatus(t *testing.T) {
	agent := SarsaEpsilonGreedy{}
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{}},
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 2, Col: 10}},
		{c.Cell{Row: 10, Col: 2}},
		{c.Cell{Row: 10, Col: 10}},
	}

	for i, test := range testTable {
		agent.SetStatus(test.cell)

		if test.cell != agent.Status {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), agent.Status, reflect.TypeOf(agent.Status))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, agent.Status)
	}
}

func TestSarsaEpsilonGreedy_BestAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect c.Action
	}{
		{c.QValues{cell: {c.Up: 1}}, c.Up},
		{c.QValues{cell: {c.Right: 1}}, c.Right},
		{c.QValues{cell: {c.Down: 1}}, c.Down},
		{c.QValues{cell: {c.Left: 1}}, c.Left},
		{c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, c.Up},
		{c.QValues{cell: {}}, c.Error},
		{c.QValues{}, c.Error},
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{QValues: test.qvs}
		actual := agent.BestAction(cell)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_ChoiceAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		cell   c.Cell
		qvs    c.QValues
		rand   float64
		expect c.Action
	}{
		{cell, c.QValues{cell: {c.Up: 0}}, 0, c.Up},
		{cell, c.QValues{cell: {c.Up: 0}}, 0.9999999, c.Up},
		{cell, c.QValues{}, 0, c.Error},
		{cell, c.QValues{cell: {c.Up: 0}}, -1, c.Error},
		{cell, c.QValues{cell: {c.Up: 0}}, 1, c.Error},
		{c.Cell{}, c.QValues{cell: {c.Up: 0}}, 1, c.Error},
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{QValues: test.qvs}

		actual := agent.ChoiceAction(test.cell, test.rand)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_IsEpsilon(t *testing.T) {
	testTable := []struct {
		e      float64
		p      float64
		expect bool
	}{
		{0.1, 0, true},
		{0.1, 0.099999, true},
		{0.1, 0.1, false},
		{0, 0, false},
		{1, 0, true},
		{1, 1, false},
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{Epsilon: test.e}

		actual := agent.IsEpsilon(test.p)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_SetupRandomValue(t *testing.T) {
	agent := SarsaEpsilonGreedy{}

	testTable := []struct {
		seed   int64
		rn     int
		expect []float64
	}{
		{1, 1, []float64{0.6046602879796196}},
		{1, 2, []float64{0.6046602879796196, 0.9405090880450124}},
		{2, 2, []float64{0.16729663442585624, 0.2650543054337802}},
	}

	for i, test := range testTable {
		agent.RandomNum = test.rn
		agent.SetupRandomValue(test.seed)

		actual := agent.Randoms
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_CellAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		cell   c.Cell
		qvs    c.QValues
		e      float64
		expect c.Action
	}{
		{cell, c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, 0, c.Up},    // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, 0, c.Right}, // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, 0, c.Down},  // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, 0, c.Left},  // 最適行動
		{cell, c.QValues{cell: {c.Up: 1}}, 1, c.Up},                                      // ランダム行動
		{cell, c.QValues{cell: {c.Right: 1}}, 1, c.Right},                                // ランダム行動
		{cell, c.QValues{cell: {c.Down: 1}}, 1, c.Down},                                  // ランダム行動
		{cell, c.QValues{cell: {c.Left: 1}}, 1, c.Left},                                  // ランダム行動
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{QValues: test.qvs, Epsilon: test.e, RandomNum: 2}
		actual := agent.CellAction(cell)

		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_GetAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		cell   c.Cell
		qvs    c.QValues
		e      float64
		expect c.Action
	}{
		{cell, c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, 0, c.Up},    // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, 0, c.Right}, // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, 0, c.Down},  // 最適行動
		{cell, c.QValues{cell: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, 0, c.Left},  // 最適行動
		{cell, c.QValues{cell: {c.Up: 1}}, 1, c.Up},                                      // ランダム行動
		{cell, c.QValues{cell: {c.Right: 1}}, 1, c.Right},                                // ランダム行動
		{cell, c.QValues{cell: {c.Down: 1}}, 1, c.Down},                                  // ランダム行動
		{cell, c.QValues{cell: {c.Left: 1}}, 1, c.Left},                                  // ランダム行動
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{Status: test.cell, QValues: test.qvs, Epsilon: test.e, RandomNum: 2}
		actual := agent.GetAction()

		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaEpsilonGreedy_Move(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	alpha := 0.1
	gamma := 0.9
	testTable := []struct {
		qvs    c.QValues
		Action c.Action
		reward float64
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.015},
		{c.QValues{status: {c.Right: 1}, status.Right(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Right, 0.7, 1.015},
		{c.QValues{status: {c.Down: 1}, status.Down(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Down, 0.7, 1.015},
		{c.QValues{status: {c.Left: 1}, status.Left(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Left, 0.7, 1.015},
		{c.QValues{status: {c.Up: 5}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 4.615},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, -3, 0.645},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 3, 1.245},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 3, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.24},
	}

	for i, test := range testTable {
		agent := SarsaEpsilonGreedy{QValues: test.qvs, Status: status, Alpha: alpha, Gamma: gamma, RandomNum: 1}
		agent.Move(test.Action, test.reward)

		if status != agent.BeforeStatus {
			t.Fatalf("%s No.%d 失敗 BeforeStatusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status, reflect.TypeOf(status), agent.BeforeStatus, reflect.TypeOf(agent.BeforeStatus))
		}

		expectStatus := status.Up()
		switch test.Action {
		case c.Right:
			expectStatus = status.Right()
		case c.Down:
			expectStatus = status.Down()
		case c.Left:
			expectStatus = status.Left()
		}
		if expectStatus != agent.Status {
			t.Fatalf("%s No.%d 失敗 Statusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status.Up(), reflect.TypeOf(status.Up()), agent.Status, reflect.TypeOf(agent.Status))
		}

		actual := agent.QValues[status][test.Action]
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 Q値が期待された値ではありません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}
