package q_epsilon_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"math/rand"
	"time"
)

type QEpsilonGreedy struct {
	QValues      c.QValues  // CellでActionした際の状態行動価値
	Status       c.Cell     // 状態 = 位置
	BeforeStatus c.Cell     // 行動前の状態 = 1つ前の位置
	Choice       []c.Action // Statusで可能な行動の選択肢
	Alpha        float64    // 学習率
	Epsilon      float64    // ランダム率
	Gamma        float64    // 割引率
	Randoms      []float64  // ランダム値用のパラメータ
	RandomNum    int        // ランダム値を何個用意しておくか
}

func (a QEpsilonGreedy) GetStatus() c.Cell {
	return a.Status
}

func (a *QEpsilonGreedy) SetStatus(c c.Cell) {
	a.Status = c
}

func (a QEpsilonGreedy) GetAction() c.Action {
	a.SetupRandomValue(time.Now().UnixNano())

	if a.IsEpsilon(a.Randoms[0]) { // ランダム行動
		return a.ChoiceAction(a.Randoms[1])
	}

	return a.BestAction()
}

func (a QEpsilonGreedy) IsEpsilon(r float64) bool {
	if 0 <= r && r < a.Epsilon {
		return true
	}
	return false
}

func (a QEpsilonGreedy) ChoiceAction(rand float64) c.Action {
	num := math.Floor(rand * float64(len(a.Choice)))
	if 0 <= num && num <= float64(len(a.Choice))-1 {
		return a.Choice[int(num)]
	}

	return c.Error
}

func (a QEpsilonGreedy) BestAction() c.Action {
	action := c.Error
	max := float64(math.MinInt32)

	for act, value := range a.QValues[a.Status] {
		if max < value {
			action = act
			max = value
		}
	}

	return action
}

func (a *QEpsilonGreedy) SetupRandomValue(seed int64) {
	rand.Seed(seed)

	a.Randoms = make([]float64, 0)
	for i := 0; i < a.RandomNum; i++ {
		a.Randoms = append(a.Randoms, rand.Float64())
	}
}

func (a *QEpsilonGreedy) Move(action c.Action, reward float64) {
	qv := a.QValues[a.Status][action]

	a.BeforeStatus = a.Status

	a.Status = a.Status.Moved(action)

	a.QValues[a.BeforeStatus][action] = qv + a.Alpha*(reward+a.Gamma*a.GetMaxQValue(a.Status)-qv)

	a.Choice = make([]c.Action, 0)
	for k := range a.QValues[a.Status] {
		a.Choice = append(a.Choice, k)
	}
}

func (a QEpsilonGreedy) GetMaxQValue(cell c.Cell) float64 {
	max := float64(math.MinInt32)

	for _, value := range a.QValues[cell] {
		if max < value {
			max = value
		}
	}

	return max
}
