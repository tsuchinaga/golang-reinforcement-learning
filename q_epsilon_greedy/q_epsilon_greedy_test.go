package q_epsilon_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"reflect"
	"testing"
)

func TestQEpsilonGreedy_GetStatus(t *testing.T) {
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 1, Col: 2}},
		{c.Cell{}},
		{c.Cell{Row: 10, Col: 20}},
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{Status: test.cell}
		actual := agent.GetStatus()

		if test.cell != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQGreedy_SetStatus(t *testing.T) {
	agent := QEpsilonGreedy{}
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{}},
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 2, Col: 10}},
		{c.Cell{Row: 10, Col: 2}},
		{c.Cell{Row: 10, Col: 10}},
	}

	for i, test := range testTable {
		agent.SetStatus(test.cell)

		if test.cell != agent.Status {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), agent.Status, reflect.TypeOf(agent.Status))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, agent.Status)
	}
}

func TestQEpsilonGreedy_ChoiceAction(t *testing.T) {
	testTable := []struct {
		choice []c.Action
		rand   float64
		expect c.Action
	}{
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0, c.Up},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.2499999, c.Up},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.25, c.Right},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.4999999, c.Right},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.5, c.Down},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.7499999, c.Down},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.75, c.Left},
		{[]c.Action{c.Up, c.Right, c.Down, c.Left}, 0.9999999, c.Left},
		{[]c.Action{c.Up}, 0, c.Up},
		{[]c.Action{c.Up}, 0.9999999, c.Up},
		{[]c.Action{c.Up, c.Down}, 0, c.Up},
		{[]c.Action{c.Up, c.Down}, 0.4999999, c.Up},
		{[]c.Action{c.Up, c.Down}, 0.5, c.Down},
		{[]c.Action{c.Up, c.Down}, 0.9999999, c.Down},
		{[]c.Action{c.Up, c.Right, c.Down}, 0, c.Up},
		{[]c.Action{c.Up, c.Right, c.Down}, 0.3, c.Up},
		{[]c.Action{c.Up, c.Right, c.Down}, 0.4, c.Right},
		{[]c.Action{c.Up, c.Right, c.Down}, 0.6, c.Right},
		{[]c.Action{c.Up, c.Right, c.Down}, 0.7, c.Down},
		{[]c.Action{c.Up, c.Right, c.Down}, 0.9, c.Down},
		{[]c.Action{}, 0, c.Error},
		{[]c.Action{c.Up}, -1, c.Error},
		{[]c.Action{c.Up}, 1, c.Error},
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{Choice: test.choice}

		actual := agent.ChoiceAction(test.rand)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQEpsilonGreedy_BestAction(t *testing.T) {
	testTable := []struct {
		status  c.Cell
		qvs     c.QValues
		expects []c.Action
		times   int
	}{
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, []c.Action{c.Up}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, []c.Action{c.Right}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, []c.Action{c.Down}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, []c.Action{c.Left}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 1}}, []c.Action{c.Up, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 1}}, []c.Action{c.Right, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 1, c.Left: 1}}, []c.Action{c.Right, c.Down, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 1, c.Down: 1, c.Left: 1}}, []c.Action{c.Up, c.Right, c.Down, c.Left}, 10},
		{c.Cell{Row: 1, Col: 1}, c.QValues{c.Cell{Row: 1, Col: 1}: {c.Right: 1, c.Down: 0}}, []c.Action{c.Right}, 1},
		{c.Cell{Row: 1, Col: 1}, c.QValues{c.Cell{Row: 1, Col: 1}: {c.Right: 0, c.Down: 1}}, []c.Action{c.Down}, 1},
	}

	for i, test := range testTable {
		for j := 0; j < test.times; j++ {
			agent := QEpsilonGreedy{QValues: test.qvs, Status: test.status}
			actual := agent.BestAction()

			result := false
			for _, expect := range test.expects {
				if expect == actual {
					result = true
				}
			}

			if !result {
				t.Fatalf("%s No.%d-%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
					t.Name(), i+1, j+1, test.expects, reflect.TypeOf(test.expects), actual, reflect.TypeOf(actual))
			}

			t.Logf("%s 成功 No.%d-%d 実際:%+v", t.Name(), i+1, j+1, actual)
		}
	}
}

func TestQEpsilonGreedy_IsEpsilon(t *testing.T) {
	testTable := []struct {
		e      float64
		p      float64
		expect bool
	}{
		{0.1, 0, true},
		{0.1, 0.099999, true},
		{0.1, 0.1, false},
		{0, 0, false},
		{1, 0, true},
		{1, 1, false},
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{Epsilon: test.e}

		actual := agent.IsEpsilon(test.p)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQEpsilonGreedy_SetupRandomValue(t *testing.T) {
	agent := QEpsilonGreedy{}

	testTable := []struct {
		seed   int64
		rn     int
		expect []float64
	}{
		{1, 1, []float64{0.6046602879796196}},
		{1, 2, []float64{0.6046602879796196, 0.9405090880450124}},
		{2, 2, []float64{0.16729663442585624, 0.2650543054337802}},
	}

	for i, test := range testTable {
		agent.RandomNum = test.rn
		agent.SetupRandomValue(test.seed)

		actual := agent.Randoms
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQEpsilonGreedy_GetAction(t *testing.T) {
	testTable := []struct {
		cell   c.Cell
		qvs    c.QValues
		e      float64
		c      []c.Action
		expect c.Action
	}{
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}},
			0, []c.Action{}, c.Up}, // 最適行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}},
			0, []c.Action{}, c.Right}, // 最適行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}},
			0, []c.Action{}, c.Down}, // 最適行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}},
			0, []c.Action{}, c.Left}, // 最適行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{}, 1, []c.Action{c.Up}, c.Up},       // ランダム行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{}, 1, []c.Action{c.Right}, c.Right}, // ランダム行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{}, 1, []c.Action{c.Down}, c.Down},   // ランダム行動
		{c.Cell{Row: 2, Col: 2}, c.QValues{}, 1, []c.Action{c.Left}, c.Left},   // ランダム行動
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{Status: test.cell, QValues: test.qvs, Epsilon: test.e, Choice: test.c, RandomNum: 2}
		actual := agent.GetAction()

		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQEpsilonGreedy_GetMaxQValue(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}}, 1},
		{c.QValues{status: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, 1},
		{c.QValues{status: {}}, math.MinInt32},
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{QValues: test.qvs}
		actual := agent.GetMaxQValue(status)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQEpsilonGreedy_Move(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	alpha := 0.1
	gamma := 0.9
	testTable := []struct {
		qvs    c.QValues
		Action c.Action
		reward float64
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.015},
		{c.QValues{status: {c.Right: 1}, status.Right(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Right, 0.7, 1.015},
		{c.QValues{status: {c.Down: 1}, status.Down(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Down, 0.7, 1.015},
		{c.QValues{status: {c.Left: 1}, status.Left(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Left, 0.7, 1.015},
		{c.QValues{status: {c.Up: 5}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 4.615},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, -3, 0.645},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 3, 1.245},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 3, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.24},
	}

	for i, test := range testTable {
		agent := QEpsilonGreedy{QValues: test.qvs, Status: status, Alpha: alpha, Gamma: gamma}
		agent.Move(test.Action, test.reward)

		if status != agent.BeforeStatus {
			t.Fatalf("%s No.%d 失敗 BeforeStatusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status, reflect.TypeOf(status), agent.BeforeStatus, reflect.TypeOf(agent.BeforeStatus))
		}

		expectStatus := status.Up()
		switch test.Action {
		case c.Right:
			expectStatus = status.Right()
		case c.Down:
			expectStatus = status.Down()
		case c.Left:
			expectStatus = status.Left()
		}
		if expectStatus != agent.Status {
			t.Fatalf("%s No.%d 失敗 Statusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status.Up(), reflect.TypeOf(status.Up()), agent.Status, reflect.TypeOf(agent.Status))
		}

		actual := agent.QValues[status][test.Action]
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 Q値が期待された値ではありません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}
