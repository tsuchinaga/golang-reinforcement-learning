package common

type QValues map[Cell]map[Action]float64

type Agent interface {
	GetStatus() Cell      // 現在地
	SetStatus(Cell)       // 現在地の更新
	GetAction() Action    // 行動則
	Move(Action, float64) // 価値関数と次点の準備
}
