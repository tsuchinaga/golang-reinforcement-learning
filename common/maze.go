package common

type Action int

const (
	Up Action = iota
	Right
	Down
	Left
	Error Action = -1
)

func (a Action) Int() int {
	switch a {
	case Up:
		return 0
	case Right:
		return 1
	case Down:
		return 2
	case Left:
		return 3
	default:
		return -1
	}
}

type Cell struct {
	Row int
	Col int
}

func (c Cell) Up() Cell {
	return Cell{c.Row - 1, c.Col}
}

func (c Cell) Right() Cell {
	return Cell{c.Row, c.Col + 1}
}

func (c Cell) Down() Cell {
	return Cell{c.Row + 1, c.Col}
}

func (c Cell) Left() Cell {
	return Cell{c.Row, c.Col - 1}
}

func (c Cell) Moved(action Action) Cell {
	switch action {
	case Up:
		return c.Up()
	case Right:
		return c.Right()
	case Down:
		return c.Down()
	case Left:
		return c.Left()
	default:
		return c
	}
}

type Maze struct {
	RowNum  int
	ColNum  int
	Start   Cell
	Goal    Cell
	Rewards []float64
}

const (
	W = -5    // Wall
	A = -0.01 // Aisle
	S = 0     // Start
	G = 10    // Goal
)

func DefaultMaze() Maze {
	return Maze{
		RowNum: 10,
		ColNum: 12,
		Start:  Cell{9, 2},
		Goal:   Cell{2, 11},
		Rewards: []float64{
			W, W, W, W, W, W, W, W, W, W, W, W,
			W, A, W, W, W, W, A, A, A, A, G, W,
			W, A, A, A, W, W, A, W, W, A, W, W,
			W, A, W, A, W, A, A, A, A, A, A, W,
			W, A, W, A, A, A, W, W, A, W, A, W,
			W, A, W, A, W, A, W, W, A, W, A, W,
			W, A, W, W, W, W, W, W, A, W, A, W,
			W, A, A, A, A, A, A, A, A, W, A, W,
			W, S, A, W, W, W, W, W, A, A, A, W,
			W, W, W, W, W, W, W, W, W, W, W, W,
		},
	}
}

func (m Maze) MovableCells(r, c int) map[Action]Cell {
	cell := Cell{r, c}
	cells := make(map[Action]Cell)

	if 1 <= r-1 && r-1 <= m.RowNum {
		cells[Up] = cell.Up()
	}
	if 1 <= c+1 && c+1 <= m.ColNum {
		cells[Right] = cell.Right()
	}
	if 1 <= r+1 && r+1 <= m.RowNum {
		cells[Down] = cell.Down()
	}
	if 1 <= c-1 && c-1 <= m.ColNum {
		cells[Left] = cell.Left()
	}

	return cells
}

func (m Maze) GetIndex(r, c int) int {

	if !(1 <= r && r <= m.RowNum && 1 <= c && c <= m.ColNum) {
		return -1
	}

	return (r-1)*m.ColNum + c - 1
}

func (m Maze) GetCell(index int) Cell {

	r := index/m.ColNum + 1
	c := index%m.ColNum + 1

	if !(1 <= r && r <= m.RowNum && 1 <= c && c <= m.ColNum) {
		return Cell{}
	}

	return Cell{r, c}
}

func (m Maze) GetReward(r, c int) float64 {
	return m.Rewards[m.GetIndex(r, c)]
}
