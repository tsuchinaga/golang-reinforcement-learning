package common

import (
	"math"
	"reflect"
	"testing"
)

func TestAction(t *testing.T) {
	testTable := []struct {
		i      int
		expect Action
	}{
		{0, Up},
		{1, Right},
		{2, Down},
		{3, Left},
		{-1, Error},
	}

	for i, test := range testTable {
		actual := Action(test.i)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 期待:%+v(%+v) 実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestAction_Int(t *testing.T) {
	testTable := []struct {
		action Action
		expect int
	}{
		{Error, -1},
		{Up, 0},
		{Right, 1},
		{Down, 2},
		{Left, 3},
	}

	for i, test := range testTable {
		actual := test.action.Int()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 期待:%+v(%+v) 実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestDefaultMaze(t *testing.T) {
	expect := Maze{
		RowNum: 10,
		ColNum: 12,
		Start:  Cell{9, 2},
		Goal:   Cell{2, 11},
		Rewards: []float64{
			W, W, W, W, W, W, W, W, W, W, W, W,
			W, A, W, W, W, W, A, A, A, A, G, W,
			W, A, A, A, W, W, A, W, W, A, W, W,
			W, A, W, A, W, A, A, A, A, A, A, W,
			W, A, W, A, A, A, W, W, A, W, A, W,
			W, A, W, A, W, A, W, W, A, W, A, W,
			W, A, W, W, W, W, W, W, A, W, A, W,
			W, A, A, A, A, A, A, A, A, W, A, W,
			W, S, A, W, W, W, W, W, A, A, A, W,
			W, W, W, W, W, W, W, W, W, W, W, W,
		},
	}

	actual := DefaultMaze()
	if !reflect.DeepEqual(expect, actual) {
		t.Fatalf("%s 失敗 期待したものと違いました", t.Name())
	}

	t.Logf("%s 成功", t.Name())
}

func TestMaze_MovableCells(t *testing.T) {
	maze := DefaultMaze()
	type p struct{ r, c int }
	testTable := []struct {
		params p
		expect map[Action]Cell
	}{
		{p{2, 2}, map[Action]Cell{Up: {1, 2}, Right: {2, 3}, Down: {3, 2}, Left: {2, 1}}},
		{p{4, 4}, map[Action]Cell{Up: {3, 4}, Right: {4, 5}, Down: {5, 4}, Left: {4, 3}}},
		{p{1, 1}, map[Action]Cell{Right: {1, 2}, Down: {2, 1}}},
		{p{1, 5}, map[Action]Cell{Right: {1, 6}, Down: {2, 5}, Left: {1, 4}}},
		{p{maze.RowNum, maze.ColNum}, map[Action]Cell{Up: {maze.RowNum - 1, maze.ColNum}, Left: {maze.RowNum, maze.ColNum - 1}}},
	}

	for i, test := range testTable {
		actual := maze.MovableCells(test.params.r, test.params.c)

		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestMaze_GetIndex(t *testing.T) {
	maze := Maze{RowNum: 3, ColNum: 3}
	type p struct{ r, c int }
	testTable := []struct {
		params p
		expect int
	}{
		{p{1, 0}, -1},
		{p{1, 1}, 0},
		{p{1, 2}, 1},
		{p{1, 3}, 2},
		{p{1, 4}, -1},
		{p{-1, 1}, -1},
		{p{2, 1}, 3},
		{p{3, 1}, 6},
		{p{4, 1}, -1},
		{p{2, 2}, 4},
		{p{3, 3}, 8},
	}

	for i, test := range testTable {
		actual := maze.GetIndex(test.params.r, test.params.c)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestCell_Up(t *testing.T) {
	testTable := []struct {
		cell   Cell
		expect Cell
	}{
		{Cell{2, 2}, Cell{1, 2}},
		{Cell{3, 3}, Cell{2, 3}},
		{Cell{1, 3}, Cell{0, 3}},
		{Cell{0, 3}, Cell{-1, 3}},
	}

	for i, test := range testTable {
		actual := test.cell.Up()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestCell_Right(t *testing.T) {
	testTable := []struct {
		cell   Cell
		expect Cell
	}{
		{Cell{2, 2}, Cell{2, 3}},
		{Cell{3, 3}, Cell{3, 4}},
		{Cell{3, math.MaxInt32 - 1}, Cell{3, math.MaxInt32}},
		{Cell{3, math.MaxInt32}, Cell{3, math.MaxInt32 + 1}},
	}

	for i, test := range testTable {
		actual := test.cell.Right()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestCell_Down(t *testing.T) {
	testTable := []struct {
		cell   Cell
		expect Cell
	}{
		{Cell{2, 2}, Cell{3, 2}},
		{Cell{3, 3}, Cell{4, 3}},
		{Cell{math.MaxInt32 - 1, 3}, Cell{math.MaxInt32, 3}},
		{Cell{math.MaxInt32, 3}, Cell{math.MaxInt32 + 1, 3}},
	}

	for i, test := range testTable {
		actual := test.cell.Down()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestCell_Left(t *testing.T) {
	testTable := []struct {
		cell   Cell
		expect Cell
	}{
		{Cell{2, 2}, Cell{2, 1}},
		{Cell{3, 3}, Cell{3, 2}},
		{Cell{3, 1}, Cell{3, 0}},
		{Cell{3, 0}, Cell{3, -1}},
	}

	for i, test := range testTable {
		actual := test.cell.Left()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestCell_Moved(t *testing.T) {
	testTable := []struct {
		cell   Cell
		action Action
		expect Cell
	}{
		{Cell{2, 2}, Up, Cell{1, 2}},
		{Cell{2, 2}, Right, Cell{2, 3}},
		{Cell{2, 2}, Down, Cell{3, 2}},
		{Cell{2, 2}, Left, Cell{2, 1}},
		{Cell{2, 2}, Error, Cell{2, 2}},
	}

	for i, test := range testTable {
		actual := test.cell.Moved(test.action)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}

func TestMaze_GetCell(t *testing.T) {
	testTable := []struct {
		maze   Maze
		param  int
		expect Cell
	}{
		{Maze{RowNum: 3, ColNum: 3}, -1, Cell{0, 0}},
		{Maze{RowNum: 3, ColNum: 3}, 0, Cell{1, 1}},
		{Maze{RowNum: 3, ColNum: 3}, 1, Cell{1, 2}},
		{Maze{RowNum: 3, ColNum: 3}, 2, Cell{1, 3}},
		{Maze{RowNum: 3, ColNum: 3}, 3, Cell{2, 1}},
		{Maze{RowNum: 3, ColNum: 3}, 4, Cell{2, 2}},
		{Maze{RowNum: 3, ColNum: 3}, 5, Cell{2, 3}},
		{Maze{RowNum: 3, ColNum: 3}, 6, Cell{3, 1}},
		{Maze{RowNum: 3, ColNum: 3}, 7, Cell{3, 2}},
		{Maze{RowNum: 3, ColNum: 3}, 8, Cell{3, 3}},
		{Maze{RowNum: 3, ColNum: 3}, 9, Cell{0, 0}},
		{Maze{RowNum: 4, ColNum: 4}, 5, Cell{2, 2}},
		{Maze{RowNum: 4, ColNum: 4}, 10, Cell{3, 3}},
		{Maze{RowNum: 10, ColNum: 12}, 11, Cell{1, 12}},
		{Maze{RowNum: 10, ColNum: 12}, 13, Cell{2, 2}},
		{Maze{RowNum: 10, ColNum: 12}, 106, Cell{9, 11}},
		{Maze{RowNum: 10, ColNum: 12}, 71, Cell{6, 12}},
	}

	for i, test := range testTable {
		actual := test.maze.GetCell(test.param)
		if test.expect != actual {
			t.Fatalf("%s(%+v) No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), test.param, i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s(%+v) 成功 No.%d", t.Name(), test.param, i+1)
	}
}

func TestMaze_GetReward(t *testing.T) {
	maze := DefaultMaze()
	testTable := []struct {
		cell   Cell
		expect float64
	}{
		{Cell{1, 1}, W},
		{Cell{2, 2}, A},
		{Cell{9, 2}, S},
		{Cell{2, 11}, G},
	}

	for i, test := range testTable {
		actual := maze.GetReward(test.cell.Row, test.cell.Col)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d", t.Name(), i+1)
	}
}
