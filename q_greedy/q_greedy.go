package q_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
)

type QGreedy struct {
	QValues      c.QValues // CellでActionした際の状態行動価値
	Status       c.Cell    // 状態 = 位置
	BeforeStatus c.Cell    // 行動前の状態 = 1つ前の位置
	Alpha        float64   // 学習率
	Gamma        float64   // 割引率
}

func (a QGreedy) GetStatus() c.Cell {
	return a.Status
}

func (a *QGreedy) SetStatus(c c.Cell) {
	a.Status = c
}

func (a QGreedy) GetAction() c.Action {
	action := c.Error
	max := float64(math.MinInt32)

	for act, value := range a.QValues[a.Status] {
		if max < value {
			action = act
			max = value
		}
	}

	return action
}

func (a *QGreedy) Move(action c.Action, reward float64) {
	qv := a.QValues[a.Status][action]

	a.BeforeStatus = a.Status

	a.Status = a.Status.Moved(action)

	a.QValues[a.BeforeStatus][action] = qv + a.Alpha*(reward+a.Gamma*a.GetMaxQValue(a.Status)-qv)
}

func (a QGreedy) GetMaxQValue(cell c.Cell) float64 {
	max := float64(math.MinInt32)

	for _, value := range a.QValues[cell] {
		if max < value {
			max = value
		}
	}

	return max
}
