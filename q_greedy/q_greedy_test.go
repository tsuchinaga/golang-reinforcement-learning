package q_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"reflect"
	"testing"
)

func TestQGreedy_GetAction(t *testing.T) {
	testTable := []struct {
		status  c.Cell
		qvs     c.QValues
		expects []c.Action
		times   int
	}{
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, []c.Action{c.Up}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, []c.Action{c.Right}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, []c.Action{c.Down}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, []c.Action{c.Left}, 1},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 1}}, []c.Action{c.Up, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 1}}, []c.Action{c.Right, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 0, c.Right: 1, c.Down: 1, c.Left: 1}}, []c.Action{c.Right, c.Down, c.Left}, 10},
		{c.Cell{Row: 2, Col: 2}, c.QValues{c.Cell{Row: 2, Col: 2}: {c.Up: 1, c.Right: 1, c.Down: 1, c.Left: 1}}, []c.Action{c.Up, c.Right, c.Down, c.Left}, 10},
		{c.Cell{Row: 1, Col: 1}, c.QValues{c.Cell{Row: 1, Col: 1}: {c.Right: 1, c.Down: 0}}, []c.Action{c.Right}, 1},
		{c.Cell{Row: 1, Col: 1}, c.QValues{c.Cell{Row: 1, Col: 1}: {c.Right: 0, c.Down: 1}}, []c.Action{c.Down}, 1},
	}

	for i, test := range testTable {
		for j := 0; j < test.times; j++ {
			agent := QGreedy{QValues: test.qvs, Status: test.status}
			actual := agent.GetAction()

			result := false
			for _, expect := range test.expects {
				if expect == actual {
					result = true
				}
			}

			if !result {
				t.Fatalf("%s No.%d-%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
					t.Name(), i+1, j+1, test.expects, reflect.TypeOf(test.expects), actual, reflect.TypeOf(actual))
			}

			t.Logf("%s 成功 No.%d-%d 実際:%+v", t.Name(), i+1, j+1, actual)
		}
	}
}

func TestQGreedy_GetMaxQValue(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}}, 1},
		{c.QValues{status: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, 1},
		{c.QValues{status: {}}, math.MinInt32},
	}

	for i, test := range testTable {
		agent := QGreedy{QValues: test.qvs}
		actual := agent.GetMaxQValue(status)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQGreedy_Move(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	alpha := 0.1
	gamma := 0.9
	testTable := []struct {
		qvs    c.QValues
		Action c.Action
		reward float64
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.015},
		{c.QValues{status: {c.Right: 1}, status.Right(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Right, 0.7, 1.015},
		{c.QValues{status: {c.Down: 1}, status.Down(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Down, 0.7, 1.015},
		{c.QValues{status: {c.Left: 1}, status.Left(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Left, 0.7, 1.015},
		{c.QValues{status: {c.Up: 5}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 4.615},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, -3, 0.645},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 3, 1.245},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 3, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.24},
	}

	for i, test := range testTable {
		agent := QGreedy{QValues: test.qvs, Status: status, Alpha: alpha, Gamma: gamma}
		agent.Move(test.Action, test.reward)

		if status != agent.BeforeStatus {
			t.Fatalf("%s No.%d 失敗 BeforeStatusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status, reflect.TypeOf(status), agent.BeforeStatus, reflect.TypeOf(agent.BeforeStatus))
		}

		expectStatus := status.Up()
		switch test.Action {
		case c.Right:
			expectStatus = status.Right()
		case c.Down:
			expectStatus = status.Down()
		case c.Left:
			expectStatus = status.Left()
		}
		if expectStatus != agent.Status {
			t.Fatalf("%s No.%d 失敗 Statusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status.Up(), reflect.TypeOf(status.Up()), agent.Status, reflect.TypeOf(agent.Status))
		}

		actual := agent.QValues[status][test.Action]
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 Q値が期待された値ではありません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQGreedy_GetStatus(t *testing.T) {
	testTable := []struct {
		status c.Cell
		expect c.Cell
	}{
		{c.Cell{Row: 2, Col: 2}, c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 1, Col: 2}, c.Cell{Row: 1, Col: 2}},
		{c.Cell{}, c.Cell{Row: 0, Col: 0}},
		{c.Cell{Row: 10, Col: 20}, c.Cell{Row: 10, Col: 20}},
	}

	for i, test := range testTable {
		agent := QGreedy{Status: test.status}
		actual := agent.GetStatus()

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQGreedy_SetStatus(t *testing.T) {
	agent := QGreedy{}
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{}},
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 2, Col: 10}},
		{c.Cell{Row: 10, Col: 2}},
		{c.Cell{Row: 10, Col: 10}},
	}

	for i, test := range testTable {
		agent.SetStatus(test.cell)

		if test.cell != agent.Status {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), agent.Status, reflect.TypeOf(agent.Status))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, agent.Status)
	}
}
