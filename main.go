package main

import (
	"fmt"
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/q_epsilon_greedy"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/q_greedy"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/q_softmax"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/sarsa_epsilon_greedy"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/sarsa_greedy"
	"gitlab.com/tsuchinaga/golang-reinforcement-learning/sarsa_softmax"
	"math/rand"
	"sync"
)

var (
	// 設定
	times    = 100  // 試行回数
	episodes = 1000 // エピソード数
	steps    = 1000 // 最大ステップ数

	// 環境
	maze    = c.DefaultMaze() // 迷路
	alpha   = 0.1             // 学習率
	gamma   = 0.9             // 割引率
	epsilon = 0.1             // ランダム率

	// 最適解
	bestSolutionSteps   = 16
	bestSolutionRewards = 9.85

	wg = &sync.WaitGroup{}
)

func main() {
	type agentSetting struct {
		name  string
		agent func() c.Agent
	}
	agents := []agentSetting{
		{"QGreedy", QGreedy},
		{"QEpsilonGreedy", QEpsilonGreedy},
		{"QSoftmax", QSoftmax},
		{"SarsaGreedy", SarsaGreedy},
		{"SarsaEpsilonGreedy", SarsaEpsilonGreedy},
		{"SarsaSoftmax", SarsaSoftmax},
	}

	for _, a := range agents {
		wg.Add(1)
		go func(setting agentSetting) {
			defer wg.Done()
			fmt.Printf("start %s\n", setting.name)
			for t := 1; t <= times; t++ {

				agent := setting.agent()
				// fmt.Printf("%+v\n", agent)

				// 確実性
				isGoalAble := false
				firstGoalEpisode := episodes + 1

				// 効率性
				bestSolutionTimes := 0
				firstBestSolutionEpisode := episodes + 1
				totalRewards := 0.0
				selfBestSteps := steps + 1
				selfBestRewards := 0.0

				// 多様性
				maxCells := 0
				maxCellActions := 0

				for e := 1; e <= episodes; e++ {
					cellActions := make(map[c.Cell]map[c.Action]int) // cell -> action -> times
					rewards := 0.0
					for s := 1; s <= steps; s++ {
						if _, ok := cellActions[agent.GetStatus()]; !ok {
							cellActions[agent.GetStatus()] = make(map[c.Action]int)
						}

						action := agent.GetAction()
						if action == c.Error {
							fmt.Printf("%s,%d,%d,行動でエラーが返りました", a.name, t, e)
						}

						if _, ok := cellActions[agent.GetStatus()][action]; !ok {
							cellActions[agent.GetStatus()][action] = 0
						}
						cellActions[agent.GetStatus()][action] += 1

						movedCell := agent.GetStatus().Moved(action)
						reward := maze.GetReward(movedCell.Row, movedCell.Col)
						agent.Move(action, reward)

						rewards += reward
						totalRewards += reward
						if agent.GetStatus() == maze.Goal {
							// fmt.Printf("Time:%d, Episode:%d, Step:%d, Reward:%f\n", t, e, s, rewards)
							agent.SetStatus(maze.Start)

							isGoalAble = true
							if firstGoalEpisode > e {
								firstGoalEpisode = e
							}

							if s <= bestSolutionSteps && rewards <= bestSolutionRewards {
								bestSolutionTimes += 1
								if firstBestSolutionEpisode > e {
									firstBestSolutionEpisode = e
								}
							}

							if selfBestSteps > s {
								selfBestSteps = s
							}

							if selfBestRewards < rewards {
								selfBestRewards = rewards
							}

							totalCells := 0
							totalCellActions := 0
							for _, mai := range cellActions {
								totalCells += 1
								for range mai {
									totalCellActions += 1
								}
							}
							if maxCells < totalCells {
								maxCells = totalCells
							}

							if maxCellActions < totalCellActions {
								maxCellActions = totalCellActions
							}
							break
						}
					}
				}

				fmt.Printf("%s,%d,%t,%d,%d,%d,%f,%d,%f,%d,%d\n",
					setting.name, t, isGoalAble, firstGoalEpisode,
					bestSolutionTimes, firstBestSolutionEpisode, totalRewards, selfBestSteps, selfBestRewards,
					maxCells, maxCellActions)

			}
		}(a)
	}
	wg.Wait()
}

func QValues() c.QValues {
	qValues := c.QValues{}
	for i := range maze.Rewards {
		cell := maze.GetCell(i)
		qValues[cell] = make(map[c.Action]float64)
		for action := range maze.MovableCells(cell.Row, cell.Col) {
			qValues[cell][action] = rand.Float64()
		}
	}
	return qValues
}

func QGreedy() c.Agent {
	agent := new(q_greedy.QGreedy)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	return agent
}

func QEpsilonGreedy() c.Agent {
	agent := new(q_epsilon_greedy.QEpsilonGreedy)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	agent.Epsilon = epsilon
	agent.RandomNum = 2
	agent.Choice = []c.Action{c.Up, c.Right, c.Down, c.Left}
	return agent
}

func QSoftmax() c.Agent {
	agent := new(q_softmax.QSoftmax)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	agent.RandomNum = 1
	return agent
}

func SarsaGreedy() c.Agent {
	agent := new(sarsa_greedy.SarsaGreedy)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	return agent
}

func SarsaEpsilonGreedy() c.Agent {
	agent := new(sarsa_epsilon_greedy.SarsaEpsilonGreedy)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	agent.Epsilon = epsilon
	agent.RandomNum = 2
	return agent
}

func SarsaSoftmax() c.Agent {
	agent := new(sarsa_softmax.SarsaSoftmax)
	agent.Status = maze.Start
	agent.QValues = QValues()
	agent.Alpha = alpha
	agent.Gamma = gamma
	agent.RandomNum = 1
	return agent
}
