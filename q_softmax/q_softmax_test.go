package q_softmax

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"reflect"
	"testing"
)

func TestQSoftmax_GetStatus(t *testing.T) {
	testTable := []struct {
		status c.Cell
		expect c.Cell
	}{
		{c.Cell{Row: 2, Col: 2}, c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 1, Col: 2}, c.Cell{Row: 1, Col: 2}},
		{c.Cell{}, c.Cell{Row: 0, Col: 0}},
		{c.Cell{Row: 10, Col: 20}, c.Cell{Row: 10, Col: 20}},
	}

	for i, test := range testTable {
		agent := QSoftmax{Status: test.status}
		actual := agent.GetStatus()

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_SetStatus(t *testing.T) {
	agent := QSoftmax{}
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{}},
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 2, Col: 10}},
		{c.Cell{Row: 10, Col: 2}},
		{c.Cell{Row: 10, Col: 10}},
	}

	for i, test := range testTable {
		agent.SetStatus(test.cell)

		if test.cell != agent.Status {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), agent.Status, reflect.TypeOf(agent.Status))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, agent.Status)
	}
}

func TestQSoftmax_SoftmaxValues(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect map[c.Action]float64
	}{
		{c.QValues{status: {c.Up: 1}}, map[c.Action]float64{c.Up: 1}},
		{c.QValues{status: {c.Up: 1, c.Down: 1}}, map[c.Action]float64{c.Up: 0.5, c.Down: 0.5}},
		{c.QValues{status: {c.Up: 1, c.Right: 1, c.Down: 1, c.Left: 1}},
			map[c.Action]float64{c.Up: 0.25, c.Right: 0.25, c.Down: 0.25, c.Left: 0.25}},
		{c.QValues{status: {c.Up: 1, c.Down: 2}}, map[c.Action]float64{c.Up: 0.2689414213699951, c.Down: 0.7310585786300049}},
		{c.QValues{status: {c.Up: 1, c.Down: 2}}, map[c.Action]float64{c.Up: 0.2689414213699951, c.Down: 0.7310585786300049}},
		{c.QValues{status: {c.Up: 0.1, c.Down: 0.2}},
			map[c.Action]float64{c.Up: 0.4750208125210601, c.Down: 0.52497918747894}},
		{c.QValues{status: {c.Up: -1, c.Down: 1}},
			map[c.Action]float64{c.Up: 0.11920292202211756, c.Down: 0.8807970779778824}},
		{c.QValues{}, map[c.Action]float64{}},
		{c.QValues{status: {}}, map[c.Action]float64{}},
	}

	for i, test := range testTable {
		agent := QSoftmax{Status: status, QValues: test.qvs}
		actual := agent.SoftmaxValues()
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_ChoiceAction(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		p      float64
		expect c.Action
	}{
		{c.QValues{status: {c.Up: 1}}, 0, c.Up},
		{c.QValues{status: {c.Up: 1}}, 0.99, c.Up},
		{c.QValues{status: {c.Up: 0, c.Down: 1}}, 0.27, c.Down},
		{c.QValues{status: {c.Up: 0, c.Down: 1}}, 0.73, c.Down},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 1}}, 0.44, c.Down},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 1}}, 0.57, c.Down},
		{c.QValues{}, 0, c.Error},
		{c.QValues{status: {}}, 0, c.Error},
	}

	for i, test := range testTable {
		agent := QSoftmax{Status: status, QValues: test.qvs}
		actual := agent.ChoiceAction(test.p)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_SetupRandomValue(t *testing.T) {
	agent := QSoftmax{}

	testTable := []struct {
		seed   int64
		rn     int
		expect []float64
	}{
		{1, 1, []float64{0.6046602879796196}},
		{1, 2, []float64{0.6046602879796196, 0.9405090880450124}},
		{2, 2, []float64{0.16729663442585624, 0.2650543054337802}},
	}

	for i, test := range testTable {
		agent.RandomNum = test.rn
		agent.SetupRandomValue(test.seed)

		actual := agent.Randoms
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_GetAction(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect c.Action
	}{
		{c.QValues{status: {c.Up: 1}}, c.Up},
		{c.QValues{status: {c.Left: 1}}, c.Left},
		{c.QValues{status: {c.Down: 1}}, c.Down},
		{c.QValues{status: {c.Right: 1}}, c.Right},
	}

	for i, test := range testTable {
		agent := QSoftmax{Status: status, QValues: test.qvs, RandomNum: 1}
		actual := agent.GetAction()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_GetMaxQValue(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}}, 1},
		{c.QValues{status: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 1, c.Down: 0, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 1, c.Left: 0}}, 1},
		{c.QValues{status: {c.Up: 0, c.Right: 0, c.Down: 0, c.Left: 1}}, 1},
		{c.QValues{status: {}}, math.MinInt32},
	}

	for i, test := range testTable {
		agent := QSoftmax{QValues: test.qvs}
		actual := agent.GetMaxQValue(status)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestQSoftmax_Move(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	alpha := 0.1
	gamma := 0.9
	testTable := []struct {
		qvs    c.QValues
		Action c.Action
		reward float64
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.015},
		{c.QValues{status: {c.Right: 1}, status.Right(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Right, 0.7, 1.015},
		{c.QValues{status: {c.Down: 1}, status.Down(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Down, 0.7, 1.015},
		{c.QValues{status: {c.Left: 1}, status.Left(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Left, 0.7, 1.015},
		{c.QValues{status: {c.Up: 5}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 4.615},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, -3, 0.645},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 3, 1.245},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 3, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.24},
	}

	for i, test := range testTable {
		agent := QSoftmax{QValues: test.qvs, Status: status, Alpha: alpha, Gamma: gamma}
		agent.Move(test.Action, test.reward)

		if status != agent.BeforeStatus {
			t.Fatalf("%s No.%d 失敗 BeforeStatusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status, reflect.TypeOf(status), agent.BeforeStatus, reflect.TypeOf(agent.BeforeStatus))
		}

		expectStatus := status.Up()
		switch test.Action {
		case c.Right:
			expectStatus = status.Right()
		case c.Down:
			expectStatus = status.Down()
		case c.Left:
			expectStatus = status.Left()
		}
		if expectStatus != agent.Status {
			t.Fatalf("%s No.%d 失敗 Statusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status.Up(), reflect.TypeOf(status.Up()), agent.Status, reflect.TypeOf(agent.Status))
		}

		actual := agent.QValues[status][test.Action]
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 Q値が期待された値ではありません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}
