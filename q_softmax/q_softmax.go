package q_softmax

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"math/rand"
	"time"
)

type QSoftmax struct {
	QValues      c.QValues // CellでActionした際の状態行動価値
	Status       c.Cell    // 状態 = 位置
	BeforeStatus c.Cell    // 行動前の状態 = 1つ前の位置
	Alpha        float64   // 学習率
	Gamma        float64   // 割引率
	Randoms      []float64 // ランダム値用のパラメータ
	RandomNum    int       // ランダム値を何個用意しておくか
}

func (a QSoftmax) GetStatus() c.Cell {
	return a.Status
}

func (a *QSoftmax) SetStatus(c c.Cell) {
	a.Status = c
}

func (a QSoftmax) GetAction() c.Action {
	a.SetupRandomValue(time.Now().UnixNano())

	return a.ChoiceAction(a.Randoms[0])
}

func (a QSoftmax) ChoiceAction(r float64) c.Action {
	total := 0.0
	for act, v := range a.SoftmaxValues() {
		total += v
		if r < total {
			return act
		}
	}
	return c.Error
}

func (a QSoftmax) SoftmaxValues() map[c.Action]float64 {
	softmax := make(map[c.Action]float64)
	total := 0.0

	for _, v := range a.QValues[a.Status] {
		total += math.Exp(v)
	}

	for act, v := range a.QValues[a.Status] {
		softmax[act] = math.Exp(v) / total
	}
	return softmax
}

func (a *QSoftmax) SetupRandomValue(seed int64) {
	rand.Seed(seed)

	a.Randoms = make([]float64, 0)
	for i := 0; i < a.RandomNum; i++ {
		a.Randoms = append(a.Randoms, rand.Float64())
	}
}

func (a *QSoftmax) Move(action c.Action, reward float64) {
	qv := a.QValues[a.Status][action]

	a.BeforeStatus = a.Status

	a.Status = a.Status.Moved(action)

	a.QValues[a.BeforeStatus][action] = qv + a.Alpha*(reward+a.Gamma*a.GetMaxQValue(a.Status)-qv)
}

func (a QSoftmax) GetMaxQValue(cell c.Cell) float64 {
	max := float64(math.MinInt32)

	for _, value := range a.QValues[cell] {
		if max < value {
			max = value
		}
	}

	return max
}
