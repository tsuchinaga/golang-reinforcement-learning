package sarsa_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
)

type SarsaGreedy struct {
	QValues      c.QValues // CellでActionした際の状態行動価値
	Status       c.Cell    // 状態 = 位置
	BeforeStatus c.Cell    // 行動前の状態 = 1つ前の位置
	Alpha        float64   // 学習率
	Gamma        float64   // 割引率
}

func (a SarsaGreedy) GetStatus() c.Cell {
	return a.Status
}

func (a *SarsaGreedy) SetStatus(c c.Cell) {
	a.Status = c
}

func (a SarsaGreedy) GetAction() c.Action {
	return a.CellAction(a.Status)
}

func (a SarsaGreedy) CellAction(cell c.Cell) c.Action {
	action := c.Error
	max := float64(math.MinInt32)

	if _, ok := a.QValues[cell]; !ok {
		return action
	}

	for act, value := range a.QValues[cell] {
		if max < value {
			action = act
			max = value
		}
	}

	return action
}

func (a *SarsaGreedy) Move(action c.Action, reward float64) {
	qv := a.QValues[a.Status][action]

	a.BeforeStatus = a.Status

	a.Status = a.Status.Moved(action)

	nextQ := 0.0
	if _, ok := a.QValues[a.Status][a.CellAction(a.Status)]; ok {
		nextQ = a.QValues[a.Status][a.CellAction(a.Status)]
	}

	a.QValues[a.BeforeStatus][action] = qv + a.Alpha*(reward+a.Gamma*nextQ-qv)
}
