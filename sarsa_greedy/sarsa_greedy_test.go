package sarsa_greedy

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"reflect"
	"testing"
)

func TestSarsaGreedy_GetStatus(t *testing.T) {
	testTable := []struct {
		status c.Cell
		expect c.Cell
	}{
		{c.Cell{Row: 2, Col: 2}, c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 1, Col: 2}, c.Cell{Row: 1, Col: 2}},
		{c.Cell{}, c.Cell{Row: 0, Col: 0}},
		{c.Cell{Row: 10, Col: 20}, c.Cell{Row: 10, Col: 20}},
	}

	for i, test := range testTable {
		agent := SarsaGreedy{Status: test.status}
		actual := agent.GetStatus()

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaGreedy_SetStatus(t *testing.T) {
	agent := SarsaGreedy{}
	testTable := []struct {
		cell c.Cell
	}{
		{c.Cell{}},
		{c.Cell{Row: 2, Col: 2}},
		{c.Cell{Row: 2, Col: 10}},
		{c.Cell{Row: 10, Col: 2}},
		{c.Cell{Row: 10, Col: 10}},
	}

	for i, test := range testTable {
		agent.SetStatus(test.cell)

		if test.cell != agent.Status {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.cell, reflect.TypeOf(test.cell), agent.Status, reflect.TypeOf(agent.Status))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, agent.Status)
	}
}

func TestSarsaGreedy_CellAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		qvs    c.QValues
		expect c.Action
	}{
		{c.QValues{cell: {c.Up: 1}}, c.Up},
		{c.QValues{cell: {c.Right: 1}}, c.Right},
		{c.QValues{cell: {c.Down: 1}}, c.Down},
		{c.QValues{cell: {c.Left: 1}}, c.Left},
		{c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, c.Up},
		{c.QValues{cell: {}}, c.Error},
		{c.QValues{}, c.Error},
	}

	for i, test := range testTable {
		agent := SarsaGreedy{QValues: test.qvs}
		actual := agent.CellAction(cell)

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaGreedy_GetAction(t *testing.T) {
	cell := c.Cell{Row: 2, Col: 2}
	testTable := []struct {
		status c.Cell
		qvs    c.QValues
		expect c.Action
	}{
		{cell, c.QValues{cell: {c.Up: 1}}, c.Up},
		{cell, c.QValues{cell: {c.Right: 1}}, c.Right},
		{cell, c.QValues{cell: {c.Down: 1}}, c.Down},
		{cell, c.QValues{cell: {c.Left: 1}}, c.Left},
		{cell, c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, c.Up},
		{cell, c.QValues{cell: {}}, c.Error},
		{cell, c.QValues{}, c.Error},
		{c.Cell{}, c.QValues{cell: {c.Up: 1, c.Right: 0, c.Down: 0, c.Left: 0}}, c.Error},
	}

	for i, test := range testTable {
		agent := SarsaGreedy{Status: test.status, QValues: test.qvs}
		actual := agent.GetAction()

		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}

func TestSarsaGreedy_Move(t *testing.T) {
	status := c.Cell{Row: 2, Col: 2}
	alpha := 0.1
	gamma := 0.9
	testTable := []struct {
		qvs    c.QValues
		Action c.Action
		reward float64
		expect float64
	}{
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.015},
		{c.QValues{status: {c.Right: 1}, status.Right(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Right, 0.7, 1.015},
		{c.QValues{status: {c.Down: 1}, status.Down(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Down, 0.7, 1.015},
		{c.QValues{status: {c.Left: 1}, status.Left(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Left, 0.7, 1.015},
		{c.QValues{status: {c.Up: 5}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 4.615},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, -3, 0.645},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 0.5, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 3, 1.245},
		{c.QValues{status: {c.Up: 1}, status.Up(): {c.Up: 3, c.Right: 0.5, c.Down: 0.5, c.Left: 0.5}}, c.Up, 0.7, 1.24},
	}

	for i, test := range testTable {
		agent := SarsaGreedy{QValues: test.qvs, Status: status, Alpha: alpha, Gamma: gamma}
		agent.Move(test.Action, test.reward)

		if status != agent.BeforeStatus {
			t.Fatalf("%s No.%d 失敗 BeforeStatusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status, reflect.TypeOf(status), agent.BeforeStatus, reflect.TypeOf(agent.BeforeStatus))
		}

		expectStatus := status.Up()
		switch test.Action {
		case c.Right:
			expectStatus = status.Right()
		case c.Down:
			expectStatus = status.Down()
		case c.Left:
			expectStatus = status.Left()
		}
		if expectStatus != agent.Status {
			t.Fatalf("%s No.%d 失敗 Statusが更新されていません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, status.Up(), reflect.TypeOf(status.Up()), agent.Status, reflect.TypeOf(agent.Status))
		}

		actual := agent.QValues[status][test.Action]
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗 Q値が期待された値ではありません\n期待:%+v(%+v)\n実際:%+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}

		t.Logf("%s 成功 No.%d 実際:%+v", t.Name(), i+1, actual)
	}
}
