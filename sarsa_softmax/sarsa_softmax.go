package sarsa_softmax

import (
	c "gitlab.com/tsuchinaga/golang-reinforcement-learning/common"
	"math"
	"math/rand"
	"time"
)

type SarsaSoftmax struct {
	QValues      c.QValues // CellでActionした際の状態行動価値
	Status       c.Cell    // 状態 = 位置
	BeforeStatus c.Cell    // 行動前の状態 = 1つ前の位置
	Alpha        float64   // 学習率
	Gamma        float64   // 割引率
	Randoms      []float64 // ランダム値用のパラメータ
	RandomNum    int       // ランダム値を何個用意しておくか
}

func (a SarsaSoftmax) GetStatus() c.Cell {
	return a.Status
}

func (a *SarsaSoftmax) SetStatus(c c.Cell) {
	a.Status = c
}

func (a SarsaSoftmax) GetAction() c.Action {
	a.SetupRandomValue(time.Now().UnixNano())

	return a.ChoiceAction(a.Status, a.Randoms[0])
}

func (a SarsaSoftmax) ChoiceAction(cell c.Cell, r float64) c.Action {
	total := 0.0
	for act, v := range a.SoftmaxValues(cell) {
		total += v
		if r < total {
			return act
		}
	}
	return c.Error
}

func (a SarsaSoftmax) SoftmaxValues(cell c.Cell) map[c.Action]float64 {
	softmax := make(map[c.Action]float64)
	total := 0.0

	for _, v := range a.QValues[cell] {
		total += math.Exp(v)
	}

	for act, v := range a.QValues[cell] {
		softmax[act] = math.Exp(v) / total
	}
	return softmax
}

func (a *SarsaSoftmax) SetupRandomValue(seed int64) {
	rand.Seed(seed)

	a.Randoms = make([]float64, 0)
	for i := 0; i < a.RandomNum; i++ {
		a.Randoms = append(a.Randoms, rand.Float64())
	}
}

func (a *SarsaSoftmax) Move(action c.Action, reward float64) {
	qv := a.QValues[a.Status][action]

	a.BeforeStatus = a.Status

	a.Status = a.Status.Moved(action)

	a.SetupRandomValue(time.Now().UnixNano())
	nextQ := 0.0
	if _, ok := a.QValues[a.Status][a.ChoiceAction(a.Status, a.Randoms[0])]; ok {
		nextQ = a.QValues[a.Status][a.ChoiceAction(a.Status, a.Randoms[0])]
	}

	a.QValues[a.BeforeStatus][action] = qv + a.Alpha*(reward+a.Gamma*nextQ-qv)
}
